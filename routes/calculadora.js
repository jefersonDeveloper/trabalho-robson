const CalculadoraService = require('../service/calculadoraService');
this.calculadora = new CalculadoraService();

module.exports = server => {
	server.get('/api/calculadora/fatorial', this.calculadora.fatorial)
	server.get('/api/calculadora/fibonacci', this.calculadora.fibonacci)
	server.get('/api/calculadora/media', this.calculadora.media)
	server.get('/api/calculadora/mediana', this.calculadora.mediana)
	server.get('/api/calculadora/potenciacao', this.calculadora.potenciacao)
}