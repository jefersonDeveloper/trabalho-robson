const restify = require('restify');
const mongoose = require('mongoose');
const dotEnv = require('dotenv').config()
const config = require('./util/config');

const server = restify.createServer();

//MiddleWare
server.use(restify.plugins.queryParser());


server.listen(config.PORT, () => {
    mongoose.connect(
        config.MONGODB_URL,
        { useNewUrlParser: true }
    );
});

const db = mongoose.connection;

db.on('error', (err) => console.log(err));

db.once( 'open',() => {
    require('./routes/calculadora')(server);
    console.log(`server rodando em ${config.PORT}`)
})