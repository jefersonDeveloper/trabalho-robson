class CalculadoraService {
  fatorial(req, res, next) {
    const CalculadoraBusiness = require("../business/CalculadoraBusiness");
    var calculadora = new CalculadoraBusiness();
    calculadora.returnFatorial(req,
      function callbackFatorial(data) {
        res.json(data); next();
      }
    );
  }

  fibonacci(req, res, next) {
    const CalculadoraBusiness = require("../business/CalculadoraBusiness");
    var calculadora = new CalculadoraBusiness();
    calculadora.returnFibonacci(req,
      function callbackFibonacci(rows) {
        res.json(rows); next();
      }
    );
  }

  media(req, res, next) {
    const CalculadoraBusiness = require("../business/CalculadoraBusiness");
    var calculadora = new CalculadoraBusiness();
    calculadora.returnMedia(req,
      function callbackMedia(rows) {
        res.json(rows); next();
      }
    );
  }

  mediana(req, res, next) {
    const CalculadoraBusiness = require("../business/CalculadoraBusiness");
    var calculadora = new CalculadoraBusiness();

    calculadora.returnMediana(req,
      function callbackMediana(rows) {
        res.json(rows); next();
      }
    );
  }

  potenciacao(req, res, next) {
    const CalculadoraBusiness = require("../business/CalculadoraBusiness");
    var calculadora = new CalculadoraBusiness();

    calculadora.returnPotenciacao(req,
      function ca(rows) {
        res.json(rows); next();
      }
    );
  }
}
module.exports = CalculadoraService;