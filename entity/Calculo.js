const mongoose = require('mongoose');


const CalculoSchema = new mongoose.Schema({
    request: {
        type: String,
        require: true,
        trim: true
    },
    result:{
        type: mongoose.Schema.Types.Mixed,
        require: true,
        trim: true
    }
},{ _id : false });

const Calculo = mongoose.model('Calculo', CalculoSchema);

module.exports = Calculo;