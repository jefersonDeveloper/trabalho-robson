"use strict";
class CalculadoraBusiness {

  returnFatorial(req, callbackFatorial) {
    const Calculo = require('../entity/Calculo');
    let queryString = req.query;
    let data = queryString["data"];
    if (data < 0) {
      let calculo = new Calculo({
        request: data,
        result: "Valor deve ser maior ou igual a zero"
      });
      callbackFatorial(calculo);
    } else {
      const fatorial = this.fatorial(data)

      let calculo = new Calculo({
        request: data,
        result: fatorial
      });
      callbackFatorial(calculo);
    }
  }

  returnFibonacci(req, callbackFibonacci) {
    const Calculo = require('../entity/Calculo');
    let queryString = req.query;
    let data = queryString["data"];
    let result = this.fibonacci(parseInt(data))
    let calculo = new Calculo({
      request: data,
      result
    });
    callbackFibonacci(calculo)
  }

  returnMedia(req, callbackMedia) {
    const Calculo = require('../entity/Calculo');
    let queryString = req.query;
    let data = queryString["data"];
    let result = this.media(data)
    let calculo = new Calculo({
      request: data,
      result
    });
    callbackMedia(calculo)
  }

  returnMediana(req, callbackMediana) {
    const Calculo = require('../entity/Calculo');
    let queryString = req.query;
    let data = queryString["data"];
    let result = this.mediana(data)
    let calculo = new Calculo({
      request: data,
      result
    });
    callbackMediana(calculo)
  }

  returnPotenciacao(req, callbackPotenciacao) {
    const Calculo = require('../entity/Calculo');
    let queryString = req.query;
    let base = queryString["base"];
    let expoente = queryString["expoente"];
    let result = this.potenciacao(base, expoente)
    let calculo = new Calculo({
      request: base.concat("^", expoente),
      result
    });
    callbackPotenciacao(calculo)
  }

  fibonacci(data) {
    let i = 0, a = 1, b = 0, temp, result = [];
    while (data >= 0) {
      temp = a;
      a = a + b;
      b = temp;
      data--;
      result[i] = b;
      i++;
    }

    return result
  }

  media(data) {
    let parse = data.split(',').map(Number);
    let result = parse.reduce((a, b) => a + b, 0) / parse.length;
    return result
  }


  mediana(data) {
    let parse = data.split(',').map(Number);
    const valores = parse.slice().sort((a, b) => a - b);
    const meio = Math.floor(valores.length / 2);
    if (valores.length % 2 === 0) {
      return (valores[meio - 1] + valores[meio]) / 2;
    }
    return valores[meio];
  }

  fatorial(data) {
    if (data == 0) {
      return 1;
    }
    let fatorial = data;
    while (data > 2) {
      fatorial *= --data;
    }
    return fatorial;
  }

  potenciacao(base, expoente) {
    let resultado = 1;
    for (let i = 0; i < expoente; i++) {
      resultado *= base;
    }
    return resultado
  }
}

module.exports = CalculadoraBusiness;