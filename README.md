# Calculadora Científica

## Instalação
1. cp .env.example .env  
2. npm install
3. npm start

# Rotas da `API`:

1. http://localhost:3000/api/calculadora/fatorial?data=10

```resultado
{
  "request": "10",
  "result": 3628800
}
``` 
2. http://localhost:3000/api/calculadora/fibonacci?data=10

```resultado
{
  "request": "10",
  "result": [
    1,
    1,
    2,
    3,
    5,
    8,
    13,
    21,
    34,
    55,
    89
  ]
}
```
3. http://localhost:3000/api/calculadora/media?data=10,5,2

```resultado
{
  "request": "10,5,2",
  "result": 5.666666666666667
}
```
4. http://localhost:3000/api/calculadora/mediana?data=10,5,2

```resultado
{
  "request": "10,5,2",
  "result": 5
}
```
5. http://localhost:3000/api/calculadora/potenciacao?base=1&expoente=2

```resultado
{
  "request": "1^2",
  "result": 1
}
```
